
import org.apache.log4j.BasicConfigurator;
import java.util.*;

public class Main {
    static MyLogger conloger = new MyLogger();
    static Scanner scan = new Scanner(System.in);
    static ArrayList<Vehicles> list = new ArrayList<>();

    public static void main(String[] args) {
        menu();
    }

    private static void speedTest(Vehicles.Type type) {
        //funkcja szukająca najszybszego pojazdu
        Iterator<Vehicles> it = list.iterator();
        Vehicles topSpeed= null;
        while (it.hasNext()) {
            Vehicles i =  it.next();
            if (topSpeed == null) {
                topSpeed =  it.next();
            }
            if (Vehicles.Type.ALL != type) {
                if (i.getType().equals(type)) {
                    if (i.getMaximal_velocity() > topSpeed.getMaximal_velocity()) {
                        topSpeed = i;
                    }
                }
            } if(Vehicles.Type.ALL == type) {
                if (i.getMaximal_velocity() > topSpeed.getMaximal_velocity()) {
                    topSpeed = i;
                }
            }
        }

        conloger.writeLog("INFO","Pojazd "+topSpeed.getType()+" producenta "+topSpeed.getProducer()+" jest najszybszy"+
                "(maksymalna prętkość wynosi:"+topSpeed.getMaximal_velocity()+")");

    }



    private static void menu()  {
        Bicycle b1 = new Bicycle("KTM", Vehicles.Type.Bicycle,40);
        Bicycle b2 = new Bicycle("Cannondale", Vehicles.Type.Bicycle,50);
        Car c1 = new Car("BMW", Vehicles.Type.Car,80);
        Car c2 = new Car("AUDI", Vehicles.Type.Car,270);
        Car c3 = new Car("kia", Vehicles.Type.Car,190);
        Plane p1 = new Plane("Boeing", Vehicles.Type.Plane,800);
        Plane p2 = new Plane("Airbus", Vehicles.Type.Plane,1290);
        Ship s1 = new Ship("Hyundai Heavy Industries", Vehicles.Type.Ship,150);
        Ship s2 = new Ship("Daewoo Shipbuilding & Marine Engineering", Vehicles.Type.Ship,320);
        Ship s3 = new Ship("Marine Enginee ring", Vehicles.Type.Car,230);

        list.add(b1);
        list.add(b2);
        list.add(c1);
        list.add(c2);
        list.add(p1);
        list.add(p2);
        list.add(s1);
        list.add(s2);
        list.add(s3);
        list.add(c3);

        BasicConfigurator.configure();
        //funkcja rysująca menu
        System.out.println("Choose from the options:");
        System.out.println("CAR");
        System.out.println("BICYCLE");
        System.out.println("PLANE");
        System.out.println("SHIP");
        System.out.println("ALL");
        System.out.println("EXIT");
        String choose = scan.nextLine();
        conloger.writeLog("INFO","Wybrano " + choose);

        switch (choose){
            case "CAR":
                speedTest(Vehicles.Type.Car);
                System.out.println("");
                menu();
                break;
            case "BICYCLE":
                speedTest(Vehicles.Type.Bicycle);
                System.out.println("");
                menu();
                break;
            case "PLANE":
                speedTest(Vehicles.Type.Plane);
                System.out.println("");
                menu();
                break;
            case "SHIP":
                speedTest(Vehicles.Type.Ship);
                System.out.println("");
                menu();
                break;
            case "ALL":
                speedTest(Vehicles.Type.ALL);
                System.out.println("");
                menu();
                break;
            case "EXIT":

                break;
            default:
                System.out.println("Try again.");
                menu();
        }

    }

    }





