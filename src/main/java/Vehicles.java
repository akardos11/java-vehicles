import java.util.ArrayList;

public class Vehicles {

    private   String producer;
  private     Type type;
  private   int maximal_velocity;



    public Vehicles(String producer, Type type, int maximal_velocity) {
        this.producer = producer;
        this.type = type;
        this.maximal_velocity = maximal_velocity;
    }

    public enum Type {
        Car,
        Plane,
        Ship,
        Bicycle,
        ALL
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public int getMaximal_velocity() {
        return maximal_velocity;
    }

    public void setMaximal_velocity(int maximal_velocity) {
        this.maximal_velocity = maximal_velocity;
    }





}
