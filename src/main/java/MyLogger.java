import org.apache.log4j.Logger;


public class MyLogger {

	static final Logger logger = Logger.getLogger("logger");



	public static void writeLog(String level, String message) {
		if(level.equals("DEBUG")) logger.debug(message);
		else if(level.equals("INFO")) logger.info(message);
		else if(level.equals("WARN")) logger.warn(message);
		else if(level.equals("ERROR")) logger.error(message);
		else if(level.equals("FATAL")) logger.fatal(message);
	}

}
